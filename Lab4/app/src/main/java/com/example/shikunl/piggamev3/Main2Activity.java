/**
 * Author: Shikun Lin
 * CIS399 18SU UO
 */

package com.example.shikunl.piggamev3;


import android.os.Bundle;
import android.app.Activity;


public class Main2Activity extends Activity {

    Player player1 = new Player();
    Player player2 = new Player();
    String name1;
    String name2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);

        name1 = getIntent().getExtras().getString("playname1");
        name2 = getIntent().getExtras().getString("playname2");

        player1.setName(name1);
        player2.setName(name2);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new MainFragment2())
                .commit();




    }


    public Player getPlayer1() {
        return player1;
    }

    public String getName1() {
        return name1;
    }

    public String getName2() {
        return name2;
    }

    public Player getPlayer2() {
        return player2;
    }
}
