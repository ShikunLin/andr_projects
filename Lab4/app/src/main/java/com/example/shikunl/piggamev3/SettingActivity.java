/**
 * Author: Shikun Lin
 * CIS399 18SU UO
 */

package com.example.shikunl.piggamev3;

import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceActivity;

public class SettingActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingFragment())
                .commit();

    }
}
