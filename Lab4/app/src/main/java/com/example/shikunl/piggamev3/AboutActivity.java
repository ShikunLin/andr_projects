/**
 * Author: Shikun Lin
 * CIS399 18SU UO
 */


package com.example.shikunl.piggamev3;

import android.os.Bundle;
import android.app.Activity;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

}
