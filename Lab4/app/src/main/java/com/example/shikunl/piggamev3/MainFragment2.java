/**
 * Author: Shikun Lin
 * CIS399 18SU UO
 */

package com.example.shikunl.piggamev3;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;


public class MainFragment2 extends Fragment {
    private EditText editTextName1;
    private EditText editTextName2;
    private TextView numScore1;
    private TextView numScore2;
    private TextView resultsTextView;
    private ImageView dieImageView;
    private TextView turnPointsView;
    private TextView pointTextView;
    private Button rollDieButton;
    private Button endTurnButton;
    private Button newGameButton;
    Player player1 = new Player();
    Player player2 = new Player();

    //winner object holder
    Player winner = new Player();

    PigGame game = new PigGame();

    private Random rand = new Random();

    //final variable for saving data
    private static final String PLAYER1_NAME = "player1_name";
    private static final String PLAYER2_NAME = "player2_name";
    private static final String PLAYER1_TURN_SCORE = "turn_score1";
    private static final String PLAYER2_TURN_SCORE= "turn_score2";
    private static final String PLAYER1_SCORE = "score1";
    private static final String PLAYER2_SCORE = "score2";
    private static final String PLAYER1_WIN = "win1";
    private static final String PLAYER2_WIN = "win2";
    private static final String WINNER_NAME = "winner_name";
    private static final String WINNER_WIN = "winner_win";
    private static final String GAME = "game";
    private static final String CURRENT_TURN = "currentTurn";
    private static final String RES = "res";
    private static final String NUMBER_TURNS = "TurnNo";
    private SharedPreferences prefs;
    private boolean countEvent = false;
    private boolean doubleScore = false;
    private int numberTurns = 20;
    private int goals = 100;
    private  int TurnNo = 0;

    //0 stand for player1, 1 stand for player2
    private int currentTurn = 0;
    //holder for winner
    private boolean res;
    private Main2Activity secondActivity;


    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        // set the default values for the preferences
        PreferenceManager.setDefaultValues(getActivity(),
                R.xml.preferences, false);

        // get the default SharedPreferences object
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_main2, container, false);
        editTextName1 = view.findViewById(R.id.editTextName1);
        editTextName2 = view.findViewById(R.id.editTextName2);
        numScore1 = view.findViewById(R.id.numScore1);
        numScore2 = view.findViewById(R.id.numScore2);
        resultsTextView = view.findViewById(R.id.resultsTextView);
        dieImageView = view.findViewById(R.id.dieImageView);
        turnPointsView = view.findViewById(R.id.turnPointsView);
        pointTextView = view.findViewById(R.id.pointTextView);
        rollDieButton = view.findViewById(R.id.rollDieButton);
        endTurnButton = view.findViewById(R.id.endTurnButton);
        newGameButton = view.findViewById(R.id.newGameButton);
        secondActivity = (Main2Activity)getActivity();
        player1.setName(secondActivity.getName1());
        player2.setName(secondActivity.getName2());

        editTextName1.setText(player1.getName());
        editTextName2.setText(player2.getName());

        rollDieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num;
                //get random number for the die
                num = rand.nextInt(6) + 1;
                //display the die
                displayImage( num );

                //check the pref first
                if(countEvent){
                    if(num%2 != 0){
                        if(currentTurn == 0){
                            player1.setTurnScore(0);
                        }
                        if(currentTurn == 1){
                            player2.setTurnScore(0);
                        }
                        rollDieButton.setClickable(false);
                        rollDieButton.setVisibility(View.INVISIBLE);
                    }else{
                        //Check current turn first
                        if ( currentTurn == 0 ){

                            //check the die's number if not equal then add to
                            //current turn score. If it equals 1, then current turn
                            //score set to 0, and make rollDieButton not clickable
                            //change turn
                            if ( num != 1 ){
                                if(doubleScore){
                                    player1.setTurnScore( 2*num + player1.getTurnScore() );
                                }else{
                                    player1.setTurnScore( num + player1.getTurnScore() );
                                }
                                player1.setName( editTextName1.getText().toString() );
                                turnPointsView.setText(Integer.toString( num ) );
                                resultsTextView.setText( player1.getName()+"'s turn" );
                            }
                            if( num == 1 ){
                                player1.setTurnScore(0);
                                rollDieButton.setClickable(false);
                                rollDieButton.setVisibility(View.INVISIBLE);
                            }
                        }else{
                            //check the die's number if not equal then add to
                            //current turn score. If it equals 1, then current turn
                            //score set to 0, and make rollDieButton not clickable
                            //change turn
                            if ( num != 1 ){
                                if(doubleScore){
                                    player2.setTurnScore( 2*num + player2.getTurnScore() );
                                }else{
                                    player2.setTurnScore( num + player2.getTurnScore() );
                                }
                                player2.setName( editTextName2.getText().toString() );
                                turnPointsView.setText( Integer.toString( num ) );
                                resultsTextView.setText( player2.getName() + "'s turn");
                            }
                            if( num == 1 ){
                                player2.setTurnScore(0);
                                rollDieButton.setClickable(false);
                                rollDieButton.setVisibility(View.INVISIBLE);
                            }
                        }

                    }
                }else{
                    //Check current turn first
                    if ( currentTurn == 0 ){

                        //check the die's number if not equal then add to
                        //current turn score. If it equals 1, then current turn
                        //score set to 0, and make rollDieButton not clickable
                        //change turn
                        if ( num != 1 ){
                            if(doubleScore){
                                player1.setTurnScore( 2*num + player1.getTurnScore() );
                            }else{
                                player1.setTurnScore( num + player1.getTurnScore() );
                            }
                            player1.setName( editTextName1.getText().toString() );
                            turnPointsView.setText(Integer.toString( num ) );
                            resultsTextView.setText( player1.getName()+"'s turn" );
                        }
                        if( num == 1 ){
                            player1.setTurnScore(0);
                            rollDieButton.setClickable(false);
                            rollDieButton.setVisibility(View.INVISIBLE);
                        }
                    }else{
                        //check the die's number if not equal then add to
                        //current turn score. If it equals 1, then current turn
                        //score set to 0, and make rollDieButton not clickable
                        //change turn
                        if ( num != 1 ){
                            if(doubleScore){
                                player2.setTurnScore( 2*num + player2.getTurnScore() );
                            }else{
                                player2.setTurnScore( num + player2.getTurnScore() );
                            }
                            player2.setName( editTextName2.getText().toString() );
                            turnPointsView.setText( Integer.toString( num ) );
                            resultsTextView.setText( player2.getName() + "'s turn");
                        }
                        if( num == 1 ){
                            player2.setTurnScore(0);
                            rollDieButton.setClickable(false);
                            rollDieButton.setVisibility(View.INVISIBLE);
                        }
                    }
                }

            }
        });
        endTurnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollDieButton.setClickable(true);
                rollDieButton.setVisibility(View.VISIBLE);
                if ( currentTurn == 0 ){
                    player1.setScore( player1.getTurnScore()+player1.getScore() );
                    numScore1.setText(Integer.toString(player1.getScore()));
                    player1.setTurnScore(0);
                    currentTurn = 1;
                }else{
                    player2.setScore( player2.getTurnScore()+player2.getScore() );
                    numScore2.setText(Integer.toString(player2.getScore()));
                    player2.setTurnScore(0);
                    TurnNo = TurnNo + 1;
                    currentTurn = 0;
                }

                // Using PigGame logic to determine the winner
                winner = game.whoWon(player1,player2,currentTurn);
                res = winner.getWin();
                if( res == true ){
                    resultsTextView.setText(winner.getName()+" WIN!!!");
                    rollDieButton.setClickable(false);
                    rollDieButton.setVisibility(View.INVISIBLE);
                    endTurnButton.setClickable(false);
                    endTurnButton.setVisibility(View.INVISIBLE);
                }
                if(TurnNo == game.getTurns()){
                    resultsTextView.setText("Game Tied!");
                    rollDieButton.setClickable(false);
                    rollDieButton.setVisibility(View.INVISIBLE);
                    endTurnButton.setClickable(false);
                    endTurnButton.setVisibility(View.INVISIBLE);
                }
            }
        });
        return view;
    }

    //Determine the die's number and display the image
    private void displayImage( int dice ) {
        int id = 0;

        switch(dice)
        {
            case 1:
                id = R.drawable.die1;
                break;
            case 2:
                id = R.drawable.die2;
                break;
            case 3:
                id = R.drawable.die3;
                break;
            case 4:
                id = R.drawable.die4;
                break;
            case 5:
                id = R.drawable.die5;
                break;
            case 6:
                id = R.drawable.die6;
                break;
        }
        dieImageView.setImageResource(id);
    }
    //saving data
    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putString(PLAYER1_NAME, player1.getName());
        outState.putString(PLAYER2_NAME,player2.getName());
        outState.putInt(PLAYER1_TURN_SCORE,player1.getTurnScore());
        outState.putInt(PLAYER2_TURN_SCORE,player2.getTurnScore());
        outState.putInt(PLAYER1_SCORE,player1.getScore());
        outState.putInt(PLAYER2_SCORE,player2.getScore());
        outState.putBoolean(PLAYER1_WIN,player1.getWin());
        outState.putBoolean(PLAYER2_WIN,player2.getWin());
        outState.putString(WINNER_NAME,winner.getName());
        outState.putBoolean(WINNER_WIN,winner.getWin());
        outState.putBoolean(RES,res);
        outState.putInt(CURRENT_TURN,currentTurn);
        outState.putInt(NUMBER_TURNS,TurnNo);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();

        // get preferences
        countEvent = prefs.getBoolean("pref_even", true);
        doubleScore = prefs.getBoolean("pref_double_turn_score",true);
        numberTurns = Integer.parseInt(prefs.getString("prefTurnNumbers","0"));
        goals = Integer.parseInt(prefs.getString("prefGoals","0"));
        game.setGoals(goals);
        game.setTurns(numberTurns);

    }
}
