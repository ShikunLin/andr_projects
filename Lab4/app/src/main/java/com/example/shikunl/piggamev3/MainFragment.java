/**
 * Author: Shikun Lin
 * CIS399 18SU UO
 */


package com.example.shikunl.piggamev3;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Fragment;
import android.widget.Button;
import android.widget.EditText;


public class MainFragment extends Fragment{

    private EditText editTextName1;
    private EditText editTextName2;
    private Button newGameButton;
    private MainActivity activity;
    private boolean twoPaneLayout = false;



    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);



    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        activity = (MainActivity)getActivity();
        editTextName1 = activity.findViewById(R.id.editTextName1);
        editTextName2 = activity.findViewById(R.id.editTextName2);


        twoPaneLayout = activity.findViewById(R.id.fragment_container2) != null;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_main, container, false);
        newGameButton = view.findViewById(R.id.newGameButton);


        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name1 = editTextName1.getText().toString();
                String name2 = editTextName2.getText().toString();

                if (twoPaneLayout) {

                } else {
                    Intent intent = new Intent(getActivity(), Main2Activity.class);
                    intent.putExtra("playername1",name1 );
                    intent.putExtra("playername2",name2 );
                    startActivity(intent);
                }
            }
        });
        return view;
    }

}
