/**
 * Author: Shikun Lin
 * UO 18Summer CIS399
 * This is setting fragment which load the preferences.xml
 */


package com.example.shikunl.piggamev2;

import android.os.Bundle;
import android.preference.PreferenceFragment;



public class SettingFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}