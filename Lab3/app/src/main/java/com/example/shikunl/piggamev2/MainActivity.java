/**
 * Author: Shikun Lin
 * UO 18Summer CIS399
 *This is Second version Pig game which allow player have
 * four settings and save data when player rotate the
 * screen or switch between apps
 */

package com.example.shikunl.piggamev2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Random;
public class MainActivity extends AppCompatActivity {


    private EditText editTextName1;
    private EditText editTextName2;
    private TextView numScore1;
    private TextView numScore2;
    private TextView resultsTextView;
    private ImageView dieImageView;
    private TextView turnPointsView;
    private TextView pointTextView;
    private Button rollDieButton;
    private Button endTurnButton;
    private Button newGameButton;
    Player player1 = new Player();
    Player player2 = new Player();

    //winner object holder
    Player winner = new Player();

    PigGame game = new PigGame();

    private Random rand = new Random();

    //final variable for saving data
    private static final String PLAYER1_NAME = "player1_name";
    private static final String PLAYER2_NAME = "player2_name";
    private static final String PLAYER1_TURN_SCORE = "turn_score1";
    private static final String PLAYER2_TURN_SCORE= "turn_score2";
    private static final String PLAYER1_SCORE = "score1";
    private static final String PLAYER2_SCORE = "score2";
    private static final String PLAYER1_WIN = "win1";
    private static final String PLAYER2_WIN = "win2";
    private static final String WINNER_NAME = "winner_name";
    private static final String WINNER_WIN = "winner_win";
    private static final String GAME = "game";
    private static final String CURRENT_TURN = "currentTurn";
    private static final String RES = "res";
    private static final String NUMBER_TURNS = "TurnNo";
    private SharedPreferences prefs;
    private boolean countEvent = false;
    private boolean doubleScore = false;
    private int numberTurns = 20;
    private int goals = 100;
    private  int TurnNo = 0;

    //0 stand for player1, 1 stand for player2
    private int currentTurn = 0;
    //holder for winner
    private boolean res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextName1 = findViewById(R.id.editTextName1);
        editTextName2 = findViewById(R.id.editTextName2);
        numScore1 = findViewById(R.id.numScore1);
        numScore2 = findViewById(R.id.numScore2);
        resultsTextView = findViewById(R.id.resultsTextView);
        dieImageView = findViewById(R.id.dieImageView);
        turnPointsView = findViewById(R.id.turnPointsView);
        pointTextView = findViewById(R.id.pointTextView);
        rollDieButton = findViewById(R.id.rollDieButton);
        endTurnButton = findViewById(R.id.endTurnButton);
        newGameButton = findViewById(R.id.newGameButton);

        //restore the data if savedInstanceState is not empty
        if(savedInstanceState != null) {
            player1.setName(savedInstanceState.getString(PLAYER1_NAME));
            player1.setTurnScore(savedInstanceState.getInt(PLAYER1_TURN_SCORE));
            player1.setScore(savedInstanceState.getInt(PLAYER1_SCORE));
            player1.setWin(savedInstanceState.getBoolean(PLAYER1_WIN));
            player2.setName(savedInstanceState.getString(PLAYER2_NAME));
            player2.setTurnScore(savedInstanceState.getInt(PLAYER2_TURN_SCORE));
            player2.setScore(savedInstanceState.getInt(PLAYER2_SCORE));
            player2.setWin(savedInstanceState.getBoolean(PLAYER2_WIN));
            winner.setName(savedInstanceState.getString(WINNER_NAME));
            winner.setWin(savedInstanceState.getBoolean(WINNER_WIN));
            res = savedInstanceState.getBoolean(RES);
            currentTurn = savedInstanceState.getInt(CURRENT_TURN);
            TurnNo = savedInstanceState.getInt(NUMBER_TURNS);

            editTextName1.setText(player1.getName());
            editTextName2.setText(player2.getName());
            numScore1.setText(Integer.toString(player1.getScore()));
            numScore2.setText(Integer.toString(player2.getScore()));
            if(currentTurn == 0){
                resultsTextView.setText( player1.getName()+"'s turn" );
            }else{
                resultsTextView.setText( player2.getName()+"'s turn" );
            }


        }
        // set the default values for the preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        // get default SharedPreferences object
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        // get preferences
        countEvent = prefs.getBoolean("pref_even", true);
        doubleScore = prefs.getBoolean("pref_double_turn_score",true);
        numberTurns = Integer.parseInt(prefs.getString("prefTurnNumbers","0"));
        goals = Integer.parseInt(prefs.getString("prefGoals","0"));
        game.setGoals(goals);
        game.setTurns(numberTurns);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
            case R.id.menu_about:
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Function for rollDieButton
    public void rollDies( View v ){
        int num;
        //get random number for the die
        num = rand.nextInt(6) + 1;
        //display the die
        displayImage( num );

        //check the pref first
        if(countEvent){
            if(num%2 != 0){
                if(currentTurn == 0){
                    player1.setTurnScore(0);
                }
                if(currentTurn == 1){
                    player2.setTurnScore(0);
                }
                rollDieButton.setClickable(false);
                rollDieButton.setVisibility(View.INVISIBLE);
            }else{
                //Check current turn first
                if ( currentTurn == 0 ){

                    //check the die's number if not equal then add to
                    //current turn score. If it equals 1, then current turn
                    //score set to 0, and make rollDieButton not clickable
                    //change turn
                    if ( num != 1 ){
                        if(doubleScore){
                            player1.setTurnScore( 2*num + player1.getTurnScore() );
                        }else{
                            player1.setTurnScore( num + player1.getTurnScore() );
                        }
                        player1.setName( editTextName1.getText().toString() );
                        turnPointsView.setText(Integer.toString( num ) );
                        resultsTextView.setText( player1.getName()+"'s turn" );
                    }
                    if( num == 1 ){
                        player1.setTurnScore(0);
                        rollDieButton.setClickable(false);
                        rollDieButton.setVisibility(View.INVISIBLE);
                    }
                }else{
                    //check the die's number if not equal then add to
                    //current turn score. If it equals 1, then current turn
                    //score set to 0, and make rollDieButton not clickable
                    //change turn
                    if ( num != 1 ){
                        if(doubleScore){
                            player2.setTurnScore( 2*num + player2.getTurnScore() );
                        }else{
                            player2.setTurnScore( num + player2.getTurnScore() );
                        }
                        player2.setName( editTextName2.getText().toString() );
                        turnPointsView.setText( Integer.toString( num ) );
                        resultsTextView.setText( player2.getName() + "'s turn");
                    }
                    if( num == 1 ){
                        player2.setTurnScore(0);
                        rollDieButton.setClickable(false);
                        rollDieButton.setVisibility(View.INVISIBLE);
                    }
                }

            }
        }else{
            //Check current turn first
            if ( currentTurn == 0 ){

                //check the die's number if not equal then add to
                //current turn score. If it equals 1, then current turn
                //score set to 0, and make rollDieButton not clickable
                //change turn
                if ( num != 1 ){
                    if(doubleScore){
                        player1.setTurnScore( 2*num + player1.getTurnScore() );
                    }else{
                        player1.setTurnScore( num + player1.getTurnScore() );
                    }
                    player1.setName( editTextName1.getText().toString() );
                    turnPointsView.setText(Integer.toString( num ) );
                    resultsTextView.setText( player1.getName()+"'s turn" );
                }
                if( num == 1 ){
                    player1.setTurnScore(0);
                    rollDieButton.setClickable(false);
                    rollDieButton.setVisibility(View.INVISIBLE);
                }
            }else{
                //check the die's number if not equal then add to
                //current turn score. If it equals 1, then current turn
                //score set to 0, and make rollDieButton not clickable
                //change turn
                if ( num != 1 ){
                    if(doubleScore){
                        player2.setTurnScore( 2*num + player2.getTurnScore() );
                    }else{
                        player2.setTurnScore( num + player2.getTurnScore() );
                    }
                    player2.setName( editTextName2.getText().toString() );
                    turnPointsView.setText( Integer.toString( num ) );
                    resultsTextView.setText( player2.getName() + "'s turn");
                }
                if( num == 1 ){
                    player2.setTurnScore(0);
                    rollDieButton.setClickable(false);
                    rollDieButton.setVisibility(View.INVISIBLE);
                }
            }
        }


    }

    //This is function for endTurnButton
    //If player click this button, currentTurn will change
    // And display the current score for the player
    public void endTurn( View v ){
        rollDieButton.setClickable(true);
        rollDieButton.setVisibility(View.VISIBLE);
        if ( currentTurn == 0 ){
            player1.setScore( player1.getTurnScore()+player1.getScore() );
            numScore1.setText(Integer.toString(player1.getScore()));
            player1.setTurnScore(0);
            currentTurn = 1;
        }else{
            player2.setScore( player2.getTurnScore()+player2.getScore() );
            numScore2.setText(Integer.toString(player2.getScore()));
            player2.setTurnScore(0);
            TurnNo = TurnNo + 1;
            currentTurn = 0;
        }

        // Using PigGame logic to determine the winner
        winner = game.whoWon(player1,player2,currentTurn);
        res = winner.getWin();
        if( res == true ){
            resultsTextView.setText(winner.getName()+" WIN!!!");
            rollDieButton.setClickable(false);
            rollDieButton.setVisibility(View.INVISIBLE);
            endTurnButton.setClickable(false);
            endTurnButton.setVisibility(View.INVISIBLE);
        }
        if(TurnNo == game.getTurns()){
            resultsTextView.setText("Game Tied!");
            rollDieButton.setClickable(false);
            rollDieButton.setVisibility(View.INVISIBLE);
            endTurnButton.setClickable(false);
            endTurnButton.setVisibility(View.INVISIBLE);
        }

    }

    //Function for newGameButton
    //set Every score and button to original status
    public void newGame( View v ){
        player1.setName("");
        player1.setTurnScore(0);
        player1.setScore(0);
        player2.setName("");
        player2.setTurnScore(0);
        player2.setScore(0);
        rollDieButton.setClickable(true);
        rollDieButton.setVisibility(View.VISIBLE);
        endTurnButton.setClickable(true);
        endTurnButton.setVisibility(View.VISIBLE);
        numScore1.setText("");
        numScore2.setText("");
        pointTextView.setText("");
        dieImageView.setImageResource(R.drawable.die5);
        TurnNo = 0;
    }

    //Determine the die's number and display the image
    private void displayImage( int dice ) {
        int id = 0;

        switch(dice)
        {
            case 1:
                id = R.drawable.die1;
                break;
            case 2:
                id = R.drawable.die2;
                break;
            case 3:
                id = R.drawable.die3;
                break;
            case 4:
                id = R.drawable.die4;
                break;
            case 5:
                id = R.drawable.die5;
                break;
            case 6:
                id = R.drawable.die6;
                break;
        }
        dieImageView.setImageResource(id);
    }

    //saving data
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString(PLAYER1_NAME, player1.getName());
        outState.putString(PLAYER2_NAME,player2.getName());
        outState.putInt(PLAYER1_TURN_SCORE,player1.getTurnScore());
        outState.putInt(PLAYER2_TURN_SCORE,player2.getTurnScore());
        outState.putInt(PLAYER1_SCORE,player1.getScore());
        outState.putInt(PLAYER2_SCORE,player2.getScore());
        outState.putBoolean(PLAYER1_WIN,player1.getWin());
        outState.putBoolean(PLAYER2_WIN,player2.getWin());
        outState.putString(WINNER_NAME,winner.getName());
        outState.putBoolean(WINNER_WIN,winner.getWin());
        outState.putBoolean(RES,res);
        outState.putInt(CURRENT_TURN,currentTurn);
        outState.putInt(NUMBER_TURNS,TurnNo);
        super.onSaveInstanceState(outState);
    }

}
