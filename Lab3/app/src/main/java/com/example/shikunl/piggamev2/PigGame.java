/**
 * Author: Shikun Lin
 * UO 18Summer CIS399
 * This is Pig gameV2 logic, using it to determine the winner
 */

package com.example.shikunl.piggamev2;

public class PigGame {
    //holder for total number player need to get
    private int goals;

    //holder for total turns players can play
    private int turns;

    //The player who get 100 or bigger than 100, and greater than other
    //Also, they got same turn number will win the game
    public PigGame(){
        this.goals = 100;
        this.turns = 20;
    }
    public Player whoWon( Player player1, Player player2, int cur ){
        int score1 = player1.getScore();
        int score2 = player2.getScore();
        Player winner = new Player();
        if ( score1 >= this.goals && score1 > score2 && cur == 0 ) {
            player1.setWin( true );
            winner =  player1;
        }
        if ( score2 >= this.goals && score2 > score1 && cur == 0 ) {
            player2.setWin(true);
            winner =  player2;
        }
        return winner;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getGoals() {
        return goals;
    }
    public void setTurns(int turns){
        this.turns = turns;
    }

    public int getTurns() {
        return turns;
    }
}
