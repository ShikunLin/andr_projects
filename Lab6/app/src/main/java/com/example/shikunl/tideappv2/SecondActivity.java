/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is second activity for the app
 * And it contain a list view for the app. The detail for tides with
 * specific date and location.
 */
package com.example.shikunl.tideappv2;

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.ListView;

public class SecondActivity extends AppCompatActivity {

    private static final String DATE = "Date";
    private static final String DAY = "Day";
    private static final String TIME = "Time";
    private static final String HEIGHT_IN_FT = "HeightInFt";
    private static final String HIGH_LOW = "HighLow";
    private static final String CITY = "City";



    private Dal dal = new Dal(this);
    Cursor cursor = null;
    SimpleCursorAdapter adapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String location = bundle.getString("locationPicked");
        String date1 = bundle.getString("dateSelected1");
        String date2 = bundle.getString("dateSelected2");
        if(location.equals("Please choose a location:")){
            location = "charleston";
        }
        date1 = "2018/" + date1;
        date2 = "2018/" + date2;
        // Initialize the database
        dal.loadTestData("charleston");

        // Get Forecast for the default location
        location = location.replaceAll("\\s","");
        dal.loadDbFromXML(location.toLowerCase());
        cursor = dal.getForcastByDate(location.toLowerCase(),date1,date2);


        adapter = new SimpleCursorAdapter(
                this,
                R.layout.item_list_view,
                cursor,
                new String[]{DATE,
                        DAY,
                        TIME,
                        HEIGHT_IN_FT,
                        HIGH_LOW,
                        CITY
                        },
                new int[]{
                        R.id.dateView,
                        R.id.dayView,
                        R.id.timeView,
                        R.id.heightView,
                        R.id.highLowView,
                        R.id.dateDayView
                },
                0 );	// no flags

        ListView itemsListView = findViewById(R.id.tideListView);
        itemsListView.setAdapter(adapter);

    }
}
