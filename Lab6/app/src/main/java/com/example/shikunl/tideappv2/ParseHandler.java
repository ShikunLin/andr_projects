/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is parse handler for TideApp V2
 */
package com.example.shikunl.tideappv2;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseHandler extends DefaultHandler {
    private TideItems tideItems;
    private TideItem item;

    //flags for the values
    private boolean isDate = false;
    private boolean isDay = false;
    private boolean isTime = false;
    private boolean isHeightInFt = false;
    private boolean isHeightInCm = false;
    private boolean isHighLow = false;
    private boolean isStationName = false;


    public TideItems getItems() {
        return tideItems;
    }

    @Override
    public void startDocument() throws SAXException {
        tideItems = new TideItems();
    }

    @Override
    public void startElement(String namespaceURI, String localName,
                             String qName, Attributes atts) throws SAXException {

        if(qName.equals("stationname")){
            isStationName = true;
        }
        else if (qName.equals("item")) {
            item = new TideItem();
        }
        else if (qName.equals("date")) {
            isDate = true;
        }
        else if (qName.equals("day")) {
            isDay = true;
        }
        else if (qName.equals("time")) {
            isTime = true;

        }
        else if (qName.equals("pred_in_ft")) {
            isHeightInFt = true;

        }
        else if(qName.equals("pred_in_cm")){
            isHeightInCm = true;
        }
        else if(qName.equals("highlow")){
            isHighLow = true;
        }

    }

    @Override
    public void endElement(String namespaceURI, String localName,
                           String qName) throws SAXException
    {
        if (qName.equals("item")) {
            tideItems.add(item);
        }
    }
    @Override
    public void characters(char ch[], int start, int length) {
        String s = new String(ch, start, length);
        if(isStationName){
            tideItems.setCity(s);
            isStationName = false;
        }
        else if( isDate ){
            item.setDate(s);
            isDate = false;
        }
        else if( isDay ){
            item.setDay(s);
            isDay = false;
        }
        else if( isTime ){
            item.setTime(s);
            isTime = false;
        }
        else if( isHeightInFt ){
            item.setHeightInFt(s);
            isHeightInFt = false;
        }
        else if( isHeightInCm ){
            item.setHeightInCm(s);
            isHeightInCm = false;
        }
        else if( isHighLow ){
            item.setHighLow(s);
            isHighLow = false;
        }


    }
}
