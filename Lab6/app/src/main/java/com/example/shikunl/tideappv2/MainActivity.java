/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is first activity for TideAppV2 which contain
 * a spinner. date picker and a button which can start second
 * activity. And send the location can date to second activity
 */

package com.example.shikunl.tideappv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;


public class MainActivity extends AppCompatActivity{

    private final String LOCATION = "locationPicked";
    private final String DATE1 = "dateSelected1";
    private final String DATE2 = "dateSelected2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showButton = findViewById(R.id.showButton);
        final DatePicker datePicker = findViewById(R.id.datePicker);
        final Spinner locationSpinner = (Spinner)findViewById(R.id.locationSpinner);

        showButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String datePicked1 = null;
                String datePicked2 = null;
                 int pickedMonth = datePicker.getMonth()+1;
                int pickedDayOfMonth = datePicker.getDayOfMonth();
                String locationSelection = locationSpinner.getSelectedItem().toString();

                //Correct the date format
                if ( pickedMonth < 10 && pickedDayOfMonth >9){
                    datePicked1 = "0"+ Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth);
                    datePicked2 = "0"+ Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth+1);
                }
                else if( pickedDayOfMonth < 10 && pickedMonth > 9 ){
                    datePicked1 = Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth);
                    datePicked2 = Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth+1);
                }
                else if(pickedMonth < 10 && pickedDayOfMonth < 10 ){
                    datePicked1 = "0"+ Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth);
                    datePicked2 = "0"+ Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth+1);
                }else {
                    datePicked1 = Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth);
                    datePicked2 = Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth+1);
                }

                //Send the date and location to second activity
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(LOCATION,locationSelection);
                bundle.putString(DATE1,datePicked1);
                bundle.putString(DATE2,datePicked2);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }

}
