/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is helper class for creating a SQLite database
 */

package com.example.shikunl.tideappv2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TideHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "tides.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static final String FORECAST = "Forecast";
    private static final String DATE = "Date";
    private static final String DAY = "Day";
    private static final String TIME = "Time";
    private static final String HEIGHT_IN_FT = "HeightInFt";
    private static final String HEIGHT_IN_CM = "HeightInCm";
    private static final String HIGH_LOW = "HighLow";
    private static final String CITY = "City";

    private Context context = null;

    public TideHelper(Context c) {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = c;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //create a database with specific contents
        db.execSQL("CREATE TABLE " + FORECAST
                + "( _id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DATE + " TEXT,"
                + DAY + " TEXT,"
                + CITY + " TEXT,"
                + TIME + " TEXT,"
                + HEIGHT_IN_CM + " TEXT,"
                + HEIGHT_IN_FT + " TEXT,"
                + HIGH_LOW + " TEXT"
                + ")" );

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

}
