/**
 * Author:Shikun Lin
 * CIS399 UO 2018SU
 * This is helper class that can parse xml file and load
 * SQLite database
 */


package com.example.shikunl.tideappv2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Dal {

    private static final String FORECAST = "Forecast";
    private static final String DATE = "Date";
    private static final String DAY = "Day";
    private static final String TIME = "Time";
    private static final String HEIGHT_IN_FT = "HeightInFt";
    private static final String HEIGHT_IN_CM = "HeightInCm";
    private static final String HIGH_LOW = "HighLow";
    private static final String CITY = "City";

    private Context context = null;

    public Dal(Context context)
    {
        this.context = context;
    }

    // This is a temporary method for loading fixed data into the db
    public void loadTestData(String city)
    {
        // Initialize database
        TideHelper helper = new TideHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        // load the database with test data if it isn't already loaded
        if (db.rawQuery("SELECT * FROM Forecast WHERE " + CITY
                + " = " + "\""+city+"\"", null).getCount() == 0) {
            loadDbFromXML("charleston");
            loadDbFromXML("halfmoonbay");
            loadDbFromXML("suislarriver");
        }
        db.close();
    }


    // Parse the XML files and put the data in the db
    public void loadDbFromXML(String cityName) {
        // Get the data from the XML file
        String fileName = cityName +"_annual.xml";

        TideItems items = parseXmlFile(fileName);

        // Initialize database
        TideHelper helper = new TideHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        // Put weather forecast in the database
        ContentValues cv = new ContentValues();

        for(TideItem item : items)
        {

            cv.put(DATE, item.getDate());
            cv.put(DAY, item.getDay());
            cv.put(CITY,items.getCity());
            cv.put(TIME,item.getTime());
            cv.put(HEIGHT_IN_FT,item.getHeightInFt());
            cv.put(HEIGHT_IN_CM,item.getHeightInCm());
            cv.put(HIGH_LOW,item.getHighLow());
            db.insert(FORECAST, null, cv);
        }
        db.close();
    }

    public Cursor getForcastByDate(String location,String date1,String date2)
    {
        loadTestData(location);

        TideHelper helper = new TideHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();

        return db.rawQuery("SELECT * FROM Forecast WHERE " +CITY
                + " = " + "\""+location+"\""+ " AND "+"("+DATE
                + " = " + "\""+date1+"\"" + " OR " + DATE +" = " + "\""+date2+"\")" , null);

    }

    public TideItems parseXmlFile(String fileName) {
        try {
            // get the XML reader
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader xmlreader = parser.getXMLReader();

            // set content handler
            ParseHandler handler = new ParseHandler();
            xmlreader.setContentHandler(handler);

            // read the file from internal storage
            InputStream in = context.getAssets().open(fileName);

            // parse the data
            InputSource is = new InputSource(in);
            xmlreader.parse(is);

            TideItems items = handler.getItems();
            return items;
        }
        catch (Exception e) {
            Log.e("News reader", e.toString());
            return null;
        }
    }
}
