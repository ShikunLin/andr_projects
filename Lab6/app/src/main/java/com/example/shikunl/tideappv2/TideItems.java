/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is TideItems class which is ArrayList contain TideItem object
 * This can hold the value for city
 */

package com.example.shikunl.tideappv2;

import java.util.ArrayList;

public class TideItems extends ArrayList<TideItem> {
    private String city = null;
    private static final long serialVersionUID = 1L;
    public void setCity(String location){
        this.city = location;
    }

    public String getCity() {
        return city;
    }
}
