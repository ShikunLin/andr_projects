/**
 * Author: Shikun Lin
 * CIS399 UO 2018Su
 * This is TideItems class which is a ArrayList of TideItem
 */

package com.example.shikunl.tideapp;

import java.util.ArrayList;
public class TideItems extends ArrayList<TideItem> {
    private static final long serialVersionUID = 1L;
}
