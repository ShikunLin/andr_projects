/**
 * Author: Shikun Lin
 * CIS399 UO 2018Su
 * This is a parse handler for TideAppV1.
 * It can parse the data from a xml file to a arrayList in
 * java.
 */
package com.example.shikunl.tideapp;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseHandler extends DefaultHandler {
    private TideItems tideItems;
    private TideItem item;

    private boolean isDate = false;
    private boolean isDay = false;
    private boolean isTime = false;
    private boolean isHeightInFt = false;
    private boolean isHeightInCm = false;
    private boolean isHighLow = false;


    public TideItems getItems() {
        return tideItems;
    }

    @Override
    public void startDocument() throws SAXException {
        tideItems = new TideItems();
    }

    @Override
    public void startElement(String namespaceURI, String localName,
                             String qName, Attributes atts) throws SAXException {

        if (qName.equals("item")) {
            item = new TideItem();
        }
        else if (qName.equals("date")) {
            isDate = true;
        }
        else if (qName.equals("day")) {
            isDay = true;
        }
        else if (qName.equals("time")) {
            isTime = true;

        }
        else if (qName.equals("pred_in_ft")) {
            isHeightInFt = true;

        }
        else if(qName.equals("pred_in_cm")){
            isHeightInCm = true;
        }
        else if(qName.equals("highlow")){
            isHighLow = true;
        }

    }

    @Override
    public void endElement(String namespaceURI, String localName,
                           String qName) throws SAXException
    {
        if (qName.equals("item")) {
            tideItems.add(item);
        }
    }
    @Override
    public void characters(char ch[], int start, int length) {
        String s = new String(ch, start, length);
        if( isDate ){
            item.setDate(s);
            isDate = false;
        }
        else if( isDay ){
            item.setDay(s);
            isDay = false;
        }
        else if( isTime ){
            item.setTime(s);
            isTime = false;
        }
        else if( isHeightInFt ){
            item.setHeightInFt(s);
            isHeightInFt = false;
        }
        else if( isHeightInCm ){
            item.setHeightInCm(s);
            isHeightInCm = false;
        }
        else if( isHighLow ){
            item.setHighLow(s);
            isHighLow = false;
        }


    }
}
