/**
 * Author: Shikun Lin
 * CIS399 UO 2018Su
 * This is MainActivity for the TideAppV1.
 * This can list all tides as listView with the
 * correct format.
 */

package com.example.shikunl.tideapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener {

    private TideItems tideItems;
    static final String DATE = "date";
    static final String DETAIL = "detail";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FileIO io = new FileIO(getApplicationContext());
        tideItems = io.readFile();

        ArrayList<HashMap<String, String>> data = new
                ArrayList<HashMap<String, String>>();

        // Put fields from each WeatherItem into a HashMap, put the HashMaps into the ArrayList
        // Keys match the column names in the SimpleAdapter
        for (TideItem item : tideItems)
        {
            HashMap<String, String> map = new HashMap<String, String>();
            String s1;
            String s2 = "";
            String formattedDay = dayFormatter(item.getDay());

            s1 = item.getDate() + " "+ formattedDay;
            if ( item.getHighLow().equals("H") ){
                s2 = "High" + " " + item.getTime();
            }else if( item.getHighLow().equals("L") ){
                s2 = "Low" + " " + item.getTime();
            }
            map.put(DATE, s1);
            map.put(DETAIL, s2);

            data.add(map);
        }


        SimpleAdapter adapter = new SimpleAdapter(this,
                data,
                R.layout.item_list_view,
                new String[]{DATE, DETAIL},
                new int[]{R.id.dateDayView,
                        R.id.timeDetailView,
                }
        );

        // Pass the data adapter to the List View
        ListView itemsListView = (ListView)findViewById(R.id.tideListView);
        itemsListView.setAdapter(adapter);
        itemsListView.setOnItemClickListener(this);
    }

    // ** Event Handler **

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TideItem item = tideItems.get(position);
        Toast.makeText(this,
                item.getHeightInFt() + "ft," + item.getHeightInCm()
                + "cm",
                Toast.LENGTH_LONG).show();
    }

    //Helper function that can format the day.
    public String dayFormatter(String days){
        String res = "";
        if(days.equals("Mon")){
            res =  "Monday";
        }
        else if(days.equals("Tue")){
            res = "Tuesday";
        }
        else if(days.equals("Wed")){
            res = "Wednesday";
        }
        else if(days.equals("Thu")){
            res = "Thursday";
        }
        else if(days.equals("Fri")){
            res = "Friday";
        }
        else if(days.equals("Sat")){
            res = "Saturday";
        }else if(days.equals("Sun")){
            res = "Sunday";
        }
        return res;
    }
}
