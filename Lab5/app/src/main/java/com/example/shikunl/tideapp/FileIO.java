/**
 * Author: Shikun Lin
 * CIS399 UO 2018Su
 * This is FIleIO class which can read a file called
 * "tide_data_annual.xml" file
 */


package com.example.shikunl.tideapp;

import android.content.Context;
import android.util.Log;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class FileIO {
    //private final String FILENAME = "tide_data_annual.xml";
    private final String FILENAME= "tide_data_annual.xml";
    private Context context = null;

    public FileIO (Context context) {
        this.context = context;
    }

    public TideItems readFile() {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader xmlreader = parser.getXMLReader();
            ParseHandler handler = new ParseHandler();
            xmlreader.setContentHandler(handler);
            InputStream in = context.getAssets().open(FILENAME);
            InputSource is = new InputSource(in);
            xmlreader.parse(is);

            TideItems items = handler.getItems();
            return items;
        }
        catch (Exception e) {
            Log.e("Reader", e.toString());
            return null;
        }
    }
}
