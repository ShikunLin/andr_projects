/**
 * Author: Shikun Lin
 * CIS399 UO 2018Su
 * This is an TideItem class which can save the
 * data from xml file as an Tide object
 */
package com.example.shikunl.tideapp;

public class TideItem {

    //variables to save the data from xml file
    private String date;
    private String day;
    private String time;
    private String HeightInFt;
    private String HeightInCm;
    private String highLow;

    public TideItem(){
        date = null;
        day = null;
        time = null;
        HeightInCm = null;
        HeightInFt = null;
        highLow = null;
    }


    //Getter and setter for the class
    public String getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }

    public String getTime() {
        return time;
    }

    public String getHeightInCm() {
        return HeightInCm;
    }

    public String getHighLow() {
        return highLow;
    }

    public String getHeightInFt() {
        return HeightInFt;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setHeightInCm(String heightInCm) {
        HeightInCm = heightInCm;
    }

    public void setHeightInFt(String heightInFt) {
        HeightInFt = heightInFt;
    }

    public void setHighLow(String highLow) {
        this.highLow = highLow;
    }

}
