/**
 * Author: Shikun Lin
 * UO 18Summer CIS399
 *This is an app which allow two player to play
 * pig dice game.
 */

package com.example.shikunl.piggamev1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Random;
public class MainActivity extends AppCompatActivity {


    private EditText editTextName1;
    private EditText editTextName2;
    private TextView numScore1;
    private TextView numScore2;
    private TextView resultsTextView;
    private ImageView dieImageView;
    private TextView turnPointsView;
    private TextView pointTextView;
    private Button rollDieButton;
    private Button endTurnButton;
    private Button newGameButton;
    Player player1 = new Player();
    Player player2 = new Player();

    //winner object holder
    Player winner = new Player();

    PigGame game = new PigGame();

    private Random rand = new Random();

    //0 stand for player1, 1 stand for player2
    private int currentTurn = 0;
    //holder for winner
    private boolean res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextName1 = findViewById(R.id.editTextName1);
        editTextName2 = findViewById(R.id.editTextName2);
        numScore1 = findViewById(R.id.numScore1);
        numScore2 = findViewById(R.id.numScore2);
        resultsTextView = findViewById(R.id.resultsTextView);
        dieImageView = findViewById(R.id.dieImageView);
        turnPointsView = findViewById(R.id.turnPointsView);
        pointTextView = findViewById(R.id.pointTextView);
        rollDieButton = findViewById(R.id.rollDieButton);
        endTurnButton = findViewById(R.id.endTurnButton);
        newGameButton = findViewById(R.id.newGameButton);


    }


    //Function for rollDieButton
    public void rollDies( View v ){
        int num;
        //get random number for the die
        num = rand.nextInt(6) + 1;
        //display the die
        displayImage( num );

        //Check current turn first
        if ( currentTurn == 0 ){

            //check the die's number if not equal then add to
            //current turn score. If it equals 1, then current turn
            //score set to 0, and make rollDieButton not clickable
            //change turn
            if ( num != 1 ){
                player1.setName( editTextName1.getText().toString() );
                player1.setTurnScore( num + player1.getTurnScore() );
                turnPointsView.setText(Integer.toString( num ) );
                resultsTextView.setText( player1.getName()+"'s turn" );
            }
            if( num == 1 ){
                player1.setTurnScore(0);
                rollDieButton.setClickable(false);
                rollDieButton.setVisibility(View.INVISIBLE);
            }
        }else{
            //check the die's number if not equal then add to
            //current turn score. If it equals 1, then current turn
            //score set to 0, and make rollDieButton not clickable
            //change turn
            if ( num != 1 ){
                player2.setName( editTextName2.getText().toString() );
                player2.setTurnScore( num + player2.getTurnScore() );
                turnPointsView.setText( Integer.toString( num ) );
                resultsTextView.setText( player2.getName() + "'s turn");
            }
            if( num == 1 ){
                player2.setTurnScore(0);
                rollDieButton.setClickable(false);
                rollDieButton.setVisibility(View.INVISIBLE);
            }
        }


    }

    //This is function for endTurnButton
    //If player click this button, currentTurn will change
    // And display the current score for the player
    public void endTurn( View v ){
        rollDieButton.setClickable(true);
        rollDieButton.setVisibility(View.VISIBLE);
        if ( currentTurn == 0 ){
            player1.setScore( player1.getTurnScore()+player1.getScore() );
            numScore1.setText(Integer.toString(player1.getScore()));
            player1.setTurnScore(0);
            currentTurn = 1;
        }else{
            player2.setScore( player2.getTurnScore()+player2.getScore() );
            numScore2.setText(Integer.toString(player2.getScore()));
            player2.setTurnScore(0);
            currentTurn = 0;
        }

        // Using PigGame logic to determine the winner
        winner = game.whoWon(player1,player2,currentTurn);
        res = winner.getWin();
        if( res == true ){
            resultsTextView.setText(winner.getName()+" WIN!!!");
            rollDieButton.setClickable(false);
            rollDieButton.setVisibility(View.INVISIBLE);
            endTurnButton.setClickable(false);
            endTurnButton.setVisibility(View.INVISIBLE);
        }

    }

    //Function for newGameButton
    //set Every score and button to original status
    public void newGame( View v ){
        player1.setName("");
        player1.setTurnScore(0);
        player1.setScore(0);
        player2.setName("");
        player2.setTurnScore(0);
        player2.setScore(0);
        rollDieButton.setClickable(true);
        rollDieButton.setVisibility(View.VISIBLE);
        endTurnButton.setClickable(true);
        endTurnButton.setVisibility(View.VISIBLE);
        numScore1.setText("");
        numScore2.setText("");
        pointTextView.setText("");
        dieImageView.setImageResource(R.drawable.die5);
    }

    //Determine the die's number and display the image
    private void displayImage( int dice ) {
        int id = 0;

        switch(dice)
        {
            case 1:
                id = R.drawable.die1;
                break;
            case 2:
                id = R.drawable.die2;
                break;
            case 3:
                id = R.drawable.die3;
                break;
            case 4:
                id = R.drawable.die4;
                break;
            case 5:
                id = R.drawable.die5;
                break;
            case 6:
                id = R.drawable.die6;
                break;
        }
        dieImageView.setImageResource(id);
    }



}
