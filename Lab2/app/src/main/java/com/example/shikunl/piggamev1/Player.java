/**
 * Author: Shikun Lin
 * UO 18Summer CIS399
 * This is Player class which is holder for info of
 * a player.
 */

package com.example.shikunl.piggamev1;

public class Player {
    //player name and turnScore and total score
    private String name;
    private int turnScore;
    private int score;
    private boolean win;

    public Player(){
        this.name = "";
        this.turnScore = 0;
        this.score = 0;
        this.win = false;
    }


    //setters and getters
    public void setName( String s ){
        name = s;
    }
    public void setTurnScore( int n ){
        turnScore = n;
    }
    public void setScore( int n ){
        score = n;
    }
    public void setWin( boolean res ){
        win = res;
    }
    public String getName(){
        return this.name;
    }
    public int getTurnScore(){
        return this.turnScore;
    }
    public int getScore(){
        return this.score;
    }
    public boolean getWin(){
        return this.win;
    }
}
