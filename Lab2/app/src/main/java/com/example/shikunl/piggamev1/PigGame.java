/**
 * Author: Shikun Lin
 * UO 18Summer CIS399
 * This is Pig game logic, using it to determine the winner
 */

package com.example.shikunl.piggamev1;

public class PigGame {

    //The player who get 100 or bigger than 100, and greater than other
    //Also, they got same turn number will win the game
   public Player whoWon( Player player1, Player player2, int cur ){
       int score1 = player1.getScore();
       int score2 = player2.getScore();
       Player winner = new Player();
       if ( score1 >= 100 && score1 > score2 && cur == 0 ) {
           player1.setWin( true );
           winner =  player1;
       }
       if ( score2 >= 100 && score2 > score1 && cur == 0 ) {
           player2.setWin(true);
           winner =  player2;
       }
       return winner;
   }

}
