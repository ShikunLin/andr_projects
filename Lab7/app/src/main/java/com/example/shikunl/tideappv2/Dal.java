/**
 * Author:Shikun Lin
 * CIS399 UO 2018SU
 * This is helper class that can parse xml file and load
 * SQLite database
 */


package com.example.shikunl.tideappv2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import java.io.InputStream;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Dal {

    private static final String FORECAST = "Forecast";
    private static final String DATE = "Date";
    private static final String TIME = "Time";
    private static final String HEIGHT_IN_FT = "HeightInFt";
    private static final String HIGH_LOW = "HighLow";
    private static final String CITY = "City";
    private static final String STATION_ID = "StationID";
    private static final String LATITUDE = "Latitude";
    private static final String LONGITUDE = "Longitude";

    private Context context = null;

    public Dal(Context context)
    {
        this.context = context;
    }

    // Parse the XML files and put the data in the db
    public void putForecastIntoDb(TideItems items) {

        // Initialize database
        TideHelper helper = new TideHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();

        // Put weather forecast in the database
        ContentValues cv = new ContentValues();

        for(TideItem item : items)
        {

            cv.put(DATE, item.getDate());
            cv.put(CITY,items.getCity());
            cv.put(TIME,item.getTime());
            cv.put(HEIGHT_IN_FT,item.getHeightInFt());
            cv.put(HIGH_LOW,item.getHighLow());
            cv.put(STATION_ID,items.getStationID());
            cv.put(LATITUDE,items.getLatitude());
            cv.put(LONGITUDE,items.getLongitude());
            db.insert(FORECAST, null, cv);
        }
        db.close();
    }

    public Cursor getForcastByDate(String location_id,String date1,String date2)
    {

        TideHelper helper = new TideHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();

        return db.rawQuery("SELECT * FROM Forecast WHERE " +STATION_ID
                + " = " + "\""+location_id+"\""+ " AND "+"("+DATE
                + " = " + "\""+date1+"\"" + " OR " + DATE +" = " + "\""+date2+"\")" , null);

    }

    public TideItems parseStream(InputStream in) {
        try {
            // get the XML reader
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader xmlreader = parser.getXMLReader();

            // set content handler
            ParseHandler handler = new ParseHandler();
            xmlreader.setContentHandler(handler);

            // parse the data
            InputSource is = new InputSource(in);
            xmlreader.parse(is);
            TideItems items = handler.getItems();
            return items;
        }
        catch (Exception e) {
            Log.e("News reader", e.toString());
            return null;
        }
    }
}
