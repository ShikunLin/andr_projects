/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is first activity for TideAppV3 which contains
 * a spinner. date picker and a button which can start second
 * activity. And send the location can date to second activity
 */

package com.example.shikunl.tideappv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;


public class MainActivity extends AppCompatActivity{

    private final String LOCATION = "locationPicked";
    private final String DATE1 = "dateSelected1";
    private final String DATE2 = "dateSelected2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showButton = findViewById(R.id.showButton);
        final DatePicker datePicker = findViewById(R.id.datePicker);
        final Spinner locationSpinner = (Spinner)findViewById(R.id.locationSpinner);

        showButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                int pickedMonth = datePicker.getMonth()+1;
                int pickedDayOfMonth = datePicker.getDayOfMonth();
                String locationSelection = locationSpinner.getSelectedItem().toString();

                //Send the date and location to second activity
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(LOCATION,locationSelection);
                bundle.putInt(DATE1,pickedMonth);
                bundle.putInt(DATE2,pickedDayOfMonth);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }

}
