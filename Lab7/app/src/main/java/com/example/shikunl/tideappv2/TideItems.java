/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is TideItems class which is ArrayList contain TideItem object
 * This can hold the value for city
 */

package com.example.shikunl.tideappv2;

import java.util.ArrayList;

public class TideItems extends ArrayList<TideItem> {
    private String city = null;
    private String stationID = null;
    private String latitude = null;
    private String longitude = null;
    private static final long serialVersionUID = 1L;
    public void setCity(String location){
        this.city = location;
    }
    public void setStationID(String id){
        this.stationID = id;
    }
    public void setLatitude(String ld){
        this.latitude = ld;
    }
    public void setLongitude(String lgd){
        this.longitude = lgd;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getStationID() {
        return stationID;
    }

    public String getCity() {
        return city;
    }
}
