/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is second activity for the app
 * And it contain a list view for the app. The detail for tides with
 * specific date and location. This activity will get data from
 * web-server and  put the data into a database. First time to search a location
 * would run slow because it needs to download the file and parse data, and put
 * into database.
 */
package com.example.shikunl.tideappv2;

import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.widget.ListView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SecondActivity extends AppCompatActivity {

    private static final String DATE = "Date";
    private static final String TIME = "Time";
    private static final String HEIGHT_IN_FT = "HeightInFt";
    private static final String HIGH_LOW = "HighLow";
    private static final String CITY = "City";


    String id = "";
    private int pickedMonth;
    private int pickedDayOfMonth;
    String location;


    private Dal dal = new Dal(this);
    Cursor cursor = null;
    SimpleCursorAdapter adapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        location = bundle.getString("locationPicked");
        pickedMonth = bundle.getInt("dateSelected1");
        pickedDayOfMonth = bundle.getInt("dateSelected2");
        if(location.equals("Please choose a location:")){
            location = "Charleston";
        }
        String datePicked1;
        String datePicked2;
        String parsedate;

        //Correct the date format for search in database
        if ( pickedMonth < 10 && pickedDayOfMonth >9){
            datePicked1 = "0"+ Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth);
            datePicked2 = "0"+ Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth+1);
            parsedate = "0"+ Integer.toString(pickedMonth)+Integer.toString(pickedDayOfMonth);
        }
        else if( pickedDayOfMonth < 10 && pickedMonth > 9 ){
            datePicked1 = Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth);
            datePicked2 = Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth+1);
            parsedate = Integer.toString(pickedMonth)+"0"+Integer.toString(pickedDayOfMonth);
        }
        else if(pickedMonth < 10 && pickedDayOfMonth < 10 ){
            datePicked1 = "0"+ Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth);
            datePicked2 = "0"+ Integer.toString(pickedMonth)+"/"+"0"+Integer.toString(pickedDayOfMonth+1);
            parsedate = "0"+ Integer.toString(pickedMonth)+"0"+Integer.toString(pickedDayOfMonth);
        }else {
            datePicked1 = Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth);
            datePicked2 = Integer.toString(pickedMonth)+"/"+Integer.toString(pickedDayOfMonth+1);
            parsedate = Integer.toString(pickedMonth)+Integer.toString(pickedDayOfMonth);
        }
        datePicked1 = datePicked1+"/2018";
        datePicked2 = datePicked2+"/2018";
        parsedate = "2018" + parsedate;

        id = formatStationID(location);

        getForecast(id,datePicked1,datePicked2,parsedate);


    }

    //This function can return a station id from a city's name
    private String formatStationID(String local){
        String id = "";
        if(local.equals("Suislaw River")){
            id = "9434098";
        }
        else if(local.equals("Half Moon Bay")){
            id = "9433445";
        }
        else if(local.equals("Charleston")){
            id = "9432780";
        }
        return id;
    }

    //get the cursor from database if database already had data, then display it. If
    //not, then using RestTask() to get data into database on background
    private void getForecast(String id, String date1, String date2, String parse_date) {
        // If there isn't a forecast in the db for this location (and date?), then get one from the web service
        cursor = dal.getForcastByDate(id,date1,date2);
        if (cursor.getCount() == 0) {
            // Get a forecast from the web service, put it in the dB, get it back out again, and display it
            new RestTask().execute(id, date1, date2, parse_date);
        } else {
            displayForecast();
        }
    }

    //Using list view to display the tides
    private void displayForecast(){

        adapter = new SimpleCursorAdapter(
                this,
                R.layout.item_list_view,
                cursor,
                new String[]{DATE,
                        TIME,
                        HEIGHT_IN_FT,
                        HIGH_LOW,
                        CITY
                },
                new int[]{
                        R.id.dateView,
                        R.id.timeView,
                        R.id.heightView,
                        R.id.highLowView,
                        R.id.dateDayView
                },
                0 );	// no flags

        ListView itemsListView = findViewById(R.id.tideListView);
        itemsListView.setAdapter(adapter);
    }



    public class RestTask extends AsyncTask<String, Void, TideItems> {

        private String station_id;
        private String parse_date;
        private String date1;
        private String date2;

        @Override
        protected TideItems doInBackground(String... params) {

            String baseUrl = "https://opendap.co-ops.nos.noaa.gov/axis/webservices/highlowtidepred/";

            station_id = params[0];
            date1 = params[1];
            date2 = params[2];
            parse_date = params[3];
            String query = "response.jsp?stationId=" + station_id + "&beginDate=" + parse_date +
                    "&endDate=20181230&datum=MLLW&unit=0&timeZone=0&format=xml&Submit=Submit";


            TideItems items = null;
            try {
                URL url = new URL(baseUrl + query);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setRequestProperty("User-Agent", "TideV3");
                connection.setRequestMethod("GET");   // The HTTP request type
                connection.connect();
                InputStream in = connection.getInputStream();

                if (in != null) {
                    items = dal.parseStream(in);
                    items.setStationID(station_id);
                    items.setCity(location);

                }
                connection.disconnect();

            } catch (Exception e) {
                Log.e("tides", "doInBackground error: " + e.getLocalizedMessage());
            }

            return items;
        }



        @Override
        protected void onPostExecute(TideItems items) {
            if (items != null && items.size() != 0) {
                dal.putForecastIntoDb(items);
                cursor = dal.getForcastByDate(station_id,date1,date2);
                displayForecast();
            }
        }
    }

}
