/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is parse handler for TideApp V2
 */
package com.example.shikunl.tideappv2;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseHandler extends DefaultHandler {
    private TideItems tideItems;
    private TideItem item;

    //flags for the values
    private boolean isTime = false;
    private boolean isHeightInFt = false;
    private boolean isHighLow = false;
    private boolean isStationName = false;
    private boolean isStationID = false;
    private boolean isLatitude = false;
    private boolean isLongitude = false;
    private String dateHolder;      //date holder for date for items

    public TideItems getItems() {
        return tideItems;
    }

    @Override
    public void startDocument() throws SAXException {
        tideItems = new TideItems();
    }

    @Override
    public void startElement(String namespaceURI, String localName,
                             String qName, Attributes atts) throws SAXException {

        if(qName.equals("stationname")){
            isStationName = true;
        }
        else if(qName.equals("stationid")){
            isStationName = true;
        }
        else if (qName.equals("item")) {
            dateHolder = atts.getValue(0);
        }
        //every "data" in xml, create a new TideItem
        else if(qName.equals("data")){
            item = new TideItem();
            item.setDate(dateHolder);
        }
        else if (qName.equals("time")) {
            isTime = true;

        }
        else if (qName.equals("pred")) {
            isHeightInFt = true;

        }
        else if(qName.equals("type")){
            isHighLow = true;
        }
        else if(qName.equals("latitude")){
            isLatitude = true;
        }
        else if(qName.equals("longitude")){
            isLongitude = true;
        }

    }

    @Override
    public void endElement(String namespaceURI, String localName,
                           String qName) throws SAXException
    {
        if (qName.equals("data")) {
            tideItems.add(item);
        }
    }
    @Override
    public void characters(char ch[], int start, int length) {
        String s = new String(ch, start, length);
        if(isStationName){
            tideItems.setCity(s);
            isStationName = false;
        }
        else if(isStationID){
            tideItems.setStationID(s);
            isStationID = false;
        }
        else if( isTime ){
            item.setTime(s);
            isTime = false;
        }
        else if( isHeightInFt ){
            item.setHeightInFt(s);
            isHeightInFt = false;
        }
        else if( isHighLow ){
            item.setHighLow(s);
            isHighLow = false;
        }
        else if( isLatitude ){
            tideItems.setLatitude(s);
            isLatitude = false;
        }
        else if( isLongitude ){
            tideItems.setLongitude(s);
            isLongitude = false;
        }


    }
}
