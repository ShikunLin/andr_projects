/**
 * Author: Shikun Lin
 * CIS399 UO 2018SU
 * This is TideItem class which can save all data for a tide
 */
package com.example.shikunl.tideappv2;

public class TideItem {

    //variables to save the data from xml file
    private String date;
    private String day;
    private String time;
    private String HeightInFt;
    private String highLow;

    public TideItem(){
        date = null;
        day = null;
        time = null;
        HeightInFt = null;
        highLow = null;
    }


    //Getter and setter for the class
    public String getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }

    public String getTime() {
        return time;
    }


    public String getHighLow() {
        return highLow;
    }

    public String getHeightInFt() {
        return HeightInFt;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setHeightInFt(String heightInFt) {
        HeightInFt = heightInFt;
    }

    public void setHighLow(String highLow) {
        this.highLow = highLow;
    }

}
