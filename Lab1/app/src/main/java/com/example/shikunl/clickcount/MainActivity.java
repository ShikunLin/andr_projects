/**
 * Author: Shikun Lin
 * UO 18Summer CIS399
 * This is an app which will count number of
 * clicks. And display the number on the screen
 */

package com.example.shikunl.clickcount;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;
public class MainActivity extends AppCompatActivity {

    //TextView display the number of Clicks
    private TextView totalClicksView;

    private Button addButton;
    private Button resetButton;

    //Shared preference to save the status
    private SharedPreferences savedValues;

    //holder for number of clicks, set to 0 for original
    private int clickCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        totalClicksView = findViewById(R.id.totalClicksView);
        addButton = findViewById(R.id.addButton);
        resetButton = findViewById(R.id.resetButton);
        savedValues = getSharedPreferences("SavedValues", MODE_PRIVATE);

        //create a click listener for "Add One" Button
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //increase one per click
                clickCount += 1;
                //set the result to screen
                totalClicksView.setText(Integer.toString(clickCount));

            }
        });

        //create a click listener for "Reset" Button
        resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //reset to 0
                clickCount = 0;
                totalClicksView.setText(Integer.toString(clickCount));

            }
        });


    }
    @Override
    public void onPause() {
        // save the instance variables
        SharedPreferences.Editor editor = savedValues.edit();
        editor.putInt("clickCount", clickCount);
        editor.commit();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        // get the instance variables
        clickCount = savedValues.getInt("clickCount", 0);
        totalClicksView.setText(Integer.toString(clickCount));

    }
}
